package com.deif.sesion

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        var primera: Button =findViewById(R.id.button2)
        primera.setOnClickListener { actor2() }
    }
    fun actor2(){
        val intent = Intent(this, iniciosecion::class.java)
        startActivity(intent)
    }
}